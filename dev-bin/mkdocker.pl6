#! /usr/bin/env perl6

use v6.c;

use JSON::Fast;

#| Build a latest image, generally used for testing a new release before making
#| it official.
multi sub MAIN ("latest")
{
  my IO::Path $basedir = $*PROGRAM.parent(2);
  my %meta = $basedir.add("META6.json").slurp.&from-json;

  run <<
    docker build
    -t "{docker-tag(%meta, version => "latest")}"
    "$basedir.absolute()"
  >>;
}

#| Build a release image, which has it's tag set depending on the current
#| version specified in META6.json.
multi sub MAIN ("release")
{
  my IO::Path $basedir = $*PROGRAM.parent(2);
  my %meta = $basedir.add("META6.json").slurp.&from-json;

  run <<
    docker build
    -t "{docker-tag(%meta)}"
    "$basedir.absolute()"
  >>;
}

#| Generate the docker tag.
sub docker-tag (
  #| Hash of META6 info.
  %meta,

  #| Override for the version identifier.
  Str :$version is copy,

  --> Str
) {
  my $tag = "";

  my $name = "cpan-p6";
  $version //= %meta<version>;

  $tag ~= "%*ENV<USER>/" if %*ENV<USER>;
  $tag ~= "$name:$version";

  $tag;
}

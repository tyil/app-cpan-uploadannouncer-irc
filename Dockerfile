FROM registry.gitlab.com/tyil/docker-perl6:debian-dev-latest AS build

COPY META6.json META6.json
COPY bin bin
COPY lib lib

RUN apt update && apt install -y curl git libssl-dev
RUN zef install --deps-only --/test .

FROM registry.gitlab.com/tyil/docker-perl6:debian-latest

RUN apt update && apt install -y libssl-dev

COPY --from=build /app /app
COPY --from=build /usr/local /usr/local

RUN perl6 -c bin/bot

CMD [ "perl6", "bin/bot" ]

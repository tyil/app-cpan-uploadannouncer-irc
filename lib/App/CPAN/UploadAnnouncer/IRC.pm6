#! /usr/bin/env false

use v6.c;

use App::CPAN::UploadAnnouncer::IRC::Module;
use App::CPAN::UploadAnnouncer::IRC::Notify;
use App::CPAN::UploadAnnouncer::IRC::Social;
use Config;
use IRC::Client;
use IRC::Client::Plugin::NickServ;
use JSON::Fast;

# Disable output buffering if Docker is detected
if ("/var/docker/meta".IO.d) {
	$*ERR.out-buffer = False;
	$*OUT.out-buffer = False;
}

#| Run the IRC bot to notify channels of new Perl 6 modules uploaded to CPAN.
sub MAIN (
	#| The nickname of the IRC bot. Can be set through $IRC_NICK
	#| instead. Defaults to "cpan-updates".
	Str :$nick is copy,

	#| The username of the IRC bot. Can be set through $IRC_USER instead. If not
	#| given, or empty, the username will match the nick.
	Str :$user is copy,

	#| The description or "real" name of the bot. Can be set through
	#| IRC_REALNAME instead.
	Str :$description is copy,

	#| The server to connect to. Can be set through $IRC_SERVER instead.
	Str :$server is copy,

	#| The port to connect to. Can be set through IRC_PORT instead. Defaults to
	#| 6697.
	Int :$port is copy,

	#| Switch SSL on. Can be set through IRC_SSL instead. Defaults to True.
	Bool :$ssl is copy,

	#| A comma-seperated list of IRC channels to join. Can be set through
	#| IRC_CHANNELS instead.
	Str :$channels is copy,

	#| The channel name to output debug information. Can be set through
	#| IRC_DEBUG_CHANNEL instead.
	Str :$debug-channel is copy,

	#| The interval in which to check for updates. Can be set through
	#| IRC_UPDATE_INTERVAL instead. Defaults to 60.
	Int :$interval is copy,

	#| The directory to store cache files in. Can be set through IRC_CACHE_DIR
	#| instead. Respects XDG_CACHE_HOME if set. Defaults to
	#| ~/.cache/cpan-upload-notifier-bot.
	Str :$cache is copy,

	#| The username to use to authenticate to NickServ. Can be set through
	#| IRC_NICKSERV_USER instead.
	Str :$nickserv-user is copy,

	#| The password to use to authenticate to NickServ. Can be set through
	#| IRC_NICKSERV_PASS instead.
	Str :$nickserv-pass is copy,

	#| Set debugging output.
	Bool:D :$debug = True,
) is export {
	# Set defaults
	$nick //= %*ENV<IRC_NICK> // "cpan-updates";
	$user //= %*ENV<IRC_USER> // $nick;
	$server //= %*ENV<IRC_SERVER> // die "You need to specify a server to connect to.";
	$port //= %*ENV<IRC_PORT> // 6697;
	$ssl //= %*ENV<IRC_SSL> // True;
	$channels //= %*ENV<IRC_CHANNELS> // die "You need to specify one or more channels to connect to.";
	$interval //= %*ENV<IRC_UPDATE_INTERVAL> // 60;
	$cache //= %*ENV<IRC_CACHE_DIR> // %*ENV<XDG_CACHE_HOME> // %*ENV<HOME>.IO.add(".cache").absolute;
	$debug-channel //= %*ENV<IRC_DEBUG_CHANNEL> // Str;
	$nickserv-user //= %*ENV<IRC_NICKSERV_USER> // Str;
	$nickserv-pass //= %*ENV<IRC_NICKSERV_PASS> // Str;
	$description //= %*ENV<IRC_REALNAME> // "Notification for new Perl 6 modules on CPAN";

	my @channels = $channels.split(",");
	my $cache-dir = $cache.IO.add("cpan-upload-notifier-bot");
	my %meta = $*PROGRAM.parent(2).add("META6.json").slurp.&from-json;
	my @plugins;

	# Add the debug channel if it's not part of the given list of channels
	@channels .= push($debug-channel) if $debug-channel && $debug-channel ∉ @channels;

	# Enable plugins
	@plugins.append: App::CPAN::UploadAnnouncer::IRC::Module.new(
		:$debug-channel,
	);

	@plugins.append: App::CPAN::UploadAnnouncer::IRC::Notify.new(
		:$cache-dir,
		:$interval,
		:@channels,
		:$debug-channel,
	);

	@plugins.append: App::CPAN::UploadAnnouncer::IRC::Social.new(
		:source-url(%meta<source-url>),
		:version(%meta<version>),
	);

	if ($nickserv-user && $nickserv-pass) {
		@plugins.append: IRC::Client::Plugin::NickServ.new(
			:config(Config.new.read: {
				nickserv => {
					nickname => $nickserv-user,
					password => $nickserv-pass,
				},
			}),
		);
	}

	# Start the bot
	IRC::Client.new(
		:$nick
		:host($server)
		:username($user)
		:userreal($description)
		:@channels
		:$debug
		:@plugins
	).run;
}

=begin pod

=NAME    App::CPAN::UploadAnnouncer::IRC
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 1.0.1

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet

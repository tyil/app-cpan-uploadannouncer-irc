#! /usr/bin/env false

use v6.c;

use DBIish;

unit module App::CPAN::UploadAnnouncer::IRC::Database;

our $dbh;

our sub dbh
{
  if (!$dbh) {
    $dbh = DBIish.connect(
      %*ENV<DB_DRIVER> // "Pg",
      user => %*ENV<DB_USER> // "cpan",
      password => %*ENV<DB_PASSWORD>,
      database => %*ENV<DB_NAME> // "cpan",
      host => %*ENV<DB_HOST> // "127.0.0.1",
      port => %*ENV<DB_PORT> // 5432,
    );
  }

  $dbh
}

=begin pod

=NAME    App::CPAN::UploadAnnouncer::IRC::Database
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 1.0.1

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet

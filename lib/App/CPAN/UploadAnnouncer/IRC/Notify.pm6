#! /usr/bin/env false

use v6.c;

use App::CPAN::UploadAnnouncer::IRC::Repositories::Module;
use App::CPAN::UploadAnnouncer::IRC::Util;
use IRC::Client;
use IRC::TextColor;
use JSON::Fast;
use Terminal::ANSIColor;

unit class App::CPAN::UploadAnnouncer::IRC::Notify does IRC::Client::Plugin;

constant ModuleRepo = App::CPAN::UploadAnnouncer::IRC::Repositories::Module;

#| The directory to hold cache files. This directory will get created if it does
#| not exist yet.
has $.cache-dir;

#| A list of channels in which to broadcast notifications.
has @.channels;

#| The interval in which updates will be polled.
has $.interval;

#| The IRC channel in which to drop debug information.
has $.debug-channel;

#| The last module that has been reported to the channels.
has %!last;

#| This method will be triggered when a connection with the IRC server has been
#| established.
method irc-connected (
	#| The IRC::Client::Message object
	$e,
) {
	my $last-cache = $!cache-dir.add("last.json");
	my @heralding-queue;

	if (!$!cache-dir.d) {
		@heralding-queue.push({
			:where($!debug-channel),
			:text("Creating directory $!cache-dir.absolute()")
		}) if $!debug-channel;

		$!cache-dir.mkdir;
	}

	$last-cache.spurt(to-json(%(
		title => "N/A",
		created_at => DateTime.now,
	))) unless $last-cache.e;

	%!last = $last-cache.slurp.&from-json;
	%!last<created_at> = DateTime.new(%!last<created_at>);

	@heralding-queue.push({
		:where($!debug-channel),
		:text("Last module (from cache) is {%!last.&module-str} (%!last<created_at>)")
	}) if $!debug-channel;

	start loop {
		@heralding-queue.push({
			:where($!debug-channel),
			:text("Fetching new modules from CPAN database")
		}) if $!debug-channel;

		my $update-start = now;

		# Get an updated list of modules
		my @updates = ModuleRepo::since(%!last<created_at>);

		@heralding-queue.push({
			:where($!debug-channel),
			:text("Database lookup took {now - $update-start}s")
		}) if $!debug-channel;

		# Set a new "last" item if there were updates
		if (@updates) {
			%!last = @updates.tail;

			@heralding-queue.push({
				:where($!debug-channel),
				:text("Saving {%!last.&module-str} (%!last<created_at>) to $last-cache.absolute()")
			}) if $!debug-channel;

			$last-cache.spurt: %!last.&to-json;

			@heralding-queue.push({
				:where($!debug-channel),
				:text("Saved {%!last.&module-str} (%!last<created_at>) to $last-cache.absolute()")
			}) if $!debug-channel;
		}

		@heralding-queue.push({
			:where($!debug-channel),
			:text("No (new) updates, last is {%!last.&module-str} (%!last<created_at>)")
		}) if !@updates && $!debug-channel;

		# Notify channel of updates
		for @updates -> %module {
			my $text = "New module released to CPAN! " ~ ircstyle(%module<title>, :bold)
				~ " (%module<version>) by " ~ ircstyle(%module<author>, :green)
				;

			for @!channels -> $where {
				@heralding-queue.push: {
					:$where,
					:$text,
				};
			}
		}

		sleep($!interval);
	}

	start loop {
		FIRST {
			sleep 10;
		}

		if (@heralding-queue) {
			my %item = @heralding-queue.shift;

			$e.irc.send(
				:where(%item<where>)
				:text(%item<text>)
			);
		}

		sleep(1.1);
	}
}

=begin pod

=NAME    App::CPAN::UploadAnnouncer::IRC::Notify
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 1.0.1

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet

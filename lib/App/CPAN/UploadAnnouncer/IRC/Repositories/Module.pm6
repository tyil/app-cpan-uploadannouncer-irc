#! /usr/bin/env false

use v6.c;

use App::CPAN::UploadAnnouncer::IRC::Database;

unit module App::CPAN::UploadAnnouncer::IRC::Repositories::Module;

constant Database = App::CPAN::UploadAnnouncer::IRC::Database;

#| Retrieve the most recent module.
our sub recent (
	--> Hash
) {
	my $sth = Database::dbh.prepare(q:to/STMT/);
		SELECT
			name AS title,
			pause_id AS author,
			auth,
			api,
			version,
			created_at
		FROM modules
		WHERE pause_id != ''
		ORDER BY created_at DESC
		LIMIT 1
		STMT

		$sth.execute;
		$sth.row(:hash);
}

#| Get a list of modules that have been added since a given DateTime.
our sub since (
	#| The DateTime of the last update that was processed.
	DateTime:D $last-update,

	--> List
) {
	my $sth = Database::dbh.prepare(q:to/STMT/);
		SELECT
			name AS title,
			pause_id AS author,
			version,
			created_at
		FROM modules
		WHERE pause_id != ''
		AND created_at > ?
		ORDER BY created_at ASC
		LIMIT 10
		STMT

	$sth.execute(~$last-update);
	$sth.allrows(:array-of-hash).List
}

=begin pod

=NAME    App::CPAN::UploadAnnouncer::IRC::Repositories::Module
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 1.0.1

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet

#! /usr/bin/env false

use v6.c;

use App::CPAN::UploadAnnouncer::IRC::Repositories::Module;
use App::CPAN::UploadAnnouncer::IRC::Util;
use IRC::Client;
use IRC::TextColor;

subset RecentModuleCommand of IRC::Client::Message where { ~$_ eq "recent" };

unit class App::CPAN::UploadAnnouncer::IRC::Module does IRC::Client::Plugin;

constant ModuleRepo = App::CPAN::UploadAnnouncer::IRC::Repositories::Module;

has Str $.debug-channel;

multi method irc-to-me (
	RecentModuleCommand:D $e,
) {
	$e.irc.send(
		:where($!debug-channel)
		:text("Retrieving latest module from database for {ircstyle($e.nick, :green)}")
	);

	start {
		CATCH {
			default {
				note $_.backtrace;

				$e.irc.send(
					:where($!debug-channel)
					:text(ircstyle(~$_, :bold, :red))
				);
			}
		}

		my $module = ModuleRepo::recent.&module-str;
		my $text = "{$e.nick}: The most recent Perl 6 module on CPAN is {ircstyle($module, :bold)}";

		$e.irc.send(:where($e.channel), :$text);
	}

	# Return a Nil, otherwise the IRC::Client object will be stringified and
	# sent to the channel this feature was used in.
	Nil
}

=begin pod

=NAME    App::CPAN::UploadAnnouncer::IRC::Module
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 1.0.1

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet

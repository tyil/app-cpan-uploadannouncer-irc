#! /usr/bin/env false

use v6.c;

use IRC::Client;
use IRC::Client::Message;
use IRC::TextColor;
use JSON::Fast;

#| This subset matches all messages that match ".bots", after whitespace has
#| been trimmed.
subset BotsRequest of IRC::Client::Message where *.Str.trim eq ".bots";

#| This subset matches all messages that contain the word "thanks" and the bot's
#| current nickname.
subset Thanks of IRC::Client::Message where { $_.Str ~~ m:i/"thanks"/ && $_.Str.contains($_.server.current-nick) };

#| This subset matches all messages triggered by the bot itself.
subset MyEvent of IRC::Client::Message where { $_.nick eq $_.server.current-nick };

unit class App::CPAN::UploadAnnouncer::IRC::Social does IRC::Client::Plugin;

#| The URL to report on a .bots command.
has $.source-url;

#| The version number to report on a .bots command.
has $.version;

#| This method gets triggered whenever a message is sent to a channel.
multi method irc-privmsg-channel (
	#| The IRC event that triggered the method.
	BotsRequest:D $e,
) {
	self!report($e);
}

#| This method gets triggered whenever a message is sent to a channel.
multi method irc-privmsg-channel (
	#| The IRC event that triggered the method.
	Thanks:D $e,
) {
	"No problem!"
}

#| Report in!
method !report (
	#| The IRC event on which to trigger the report. The channel and the bot's
	#| current nick will be extracted from this.
	IRC::Client::Message:D $e
) {
	my $current-nick = $e.server.current-nick;
	my $build-date = "/var/docker/meta/build-date".IO;
	my $text;

	$text ~= ircstyle($current-nick, :green);
	$text ~= ":" ~ ircstyle($!version, :light_blue);
	$text ~= " ({DateTime.new($build-date.slurp.trim)})" if $build-date.f;
	$text ~= " reporting for duty! [Perl 6] ";
	$text ~= ircstyle($!source-url, :blue, :underline);

	$e.irc.send(:where($e.channel), :$text);
}

=begin pod

=NAME    App::CPAN::UploadAnnouncer::IRC::Social
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 1.0.1

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet

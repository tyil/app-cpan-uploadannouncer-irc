#! /usr/bin/env false

use v6.c;

use IRC::TextColor;

unit module App::CPAN::UploadAnnouncer::IRC::Util;

#| Stringify a hash representing a module.
sub module-str (
	#| A hash with module metadata.
	%module,

	#| Apply coloring to the output string?
	Bool:D :$color = True,

	--> Str
) is export {
	my $s = %module<title>;

	$s ~= ":api<%module<api>>" if %module<api>;
	$s ~= ":auth<%module<auth>>" if %module<auth>;
	$s ~= ":version<%module<version>>" if %module<version>;

	$s
}

=begin pod

=NAME    App::CPAN::UploadAnnouncer::IRC::Util
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 1.0.1

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet

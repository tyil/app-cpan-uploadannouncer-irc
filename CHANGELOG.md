# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic
Versioning](http://semver.org/spec/v2.0.0.html).

## [1.1.0] - [UNRELEASD]
- The bot's version is now reported on a `.bots` command. Additionally, the
  date the Docker image containing the bot was build is also included, if
  available.

## [1.0.1] - 2019-03-28
- The bot now reports it's sources based on the `source-url` from `META6.json`.

## [1.0.0] - 2019-03-28
- Initial release
